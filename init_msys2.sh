#!/bin/bash

curl https://gitlab.com/kmorihiro/setup/raw/master/mingw64.ini -o /mingw64.ini
curl https://gitlab.com/kmorihiro/setup/raw/master/pacman.conf -o /etc/pacman.conf
curl https://gitlab.com/kmorihiro/setup/raw/master/fstab -o /etc/fstab
mount -a

# add git-for-windows gpg key
cd /usr/share/pacman/keyrings
wget -N https://raw.githubusercontent.com/git-for-windows/build-extra/master/git-for-windows-keyring/git-for-windows.gpg https://raw.githubusercontent.com/git-for-windows/build-extra/master/git-for-windows-keyring/git-for-windows-trusted https://raw.githubusercontent.com/git-for-windows/build-extra/master/git-for-windows-keyring/git-for-windows-revoked
pacman-key --populate git-for-windows
rm git-for-windows*

pacman -Syuu --noconfirm

# setup

One-liner scripts to set up a environment.

## Installation

### msys2

(optional) Install CA certificates

```sh
cat - << EOS yourCA.cer >> /usr/ssl/certs/ca-bundle.crt

# Your CA
EOS
```

```sh
curl https://gitlab.com/kmorihiro/setup/raw/master/init_msys2.sh | bash
```

Restart a terminal, and run below.

```sh
curl https://gitlab.com/kmorihiro/setup/raw/master/install_git.sh | bash
```
